/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectompi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author eli
 */
public class Proceso {

    public String leerLinea(Process is) {
        String aux="";
        BufferedReader inStream = null;
        try {
            inStream = new BufferedReader(
                    new InputStreamReader(is.getInputStream()));
            //System.out.println(inStream.readLine());
            String line = inStream.readLine();
            while(line != null){
                aux += line +"\n";
                line = inStream.readLine();
            }
        } catch (IOException e) {
            System.err.println("Error en inStream.readLine()");
            e.printStackTrace();
        }
        return aux;
    }
    
    public Process ejecutarComando(String comando) {
        Process theProcess = null;
        
        
        System.out.println("Proceso invocado "+comando);

        // llamar a la clase Hello.
        try {
            String [] comandos = {"bash", "-c",comando};
            theProcess = Runtime.getRuntime().exec(comandos);
        } catch (IOException e) {
            System.err.println("Error en el método exec()");
            e.printStackTrace();
        }

        return theProcess;        
    }
    
    
    
    public static void main(String[] args) {
        Proceso p = new Proceso();
        //Process po = p.ejecutarComando("mpirun -np 2 /home/eli/Escritorio/mpi/hola");
        Process po = p.ejecutarComando("lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l");
        
        System.out.println();
        Integer.parseInt(p.leerLinea(po).trim());
    }
}
